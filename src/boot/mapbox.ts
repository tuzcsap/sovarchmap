import {boot} from 'quasar/wrappers';
import VueMapboxTs from 'vue-mapbox-ts';

export default boot(({app}) => {
  app.use(VueMapboxTs);
})
